<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiploma extends Migration {

	public function up()
	{
        Schema::create('table_diploma', function(Blueprint $table)
        {
            $table->increments('diploma_id');
            $table->string('user_id');
            $table->string('diploma_title');
            $table->string('diploma_about');
            $table->string('diploma_photo');
            $table->timestamps();
        });
	}

	public function down()
	{
		//
	}

}
