<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('table_users', function(Blueprint $table)
		{
			$table->increments('user_id');
            $table->string('user_fristname');
            $table->string('user_lastname');
            $table->string('user_phonenumber')->unique();
            $table->string('user_email')->unique();
            $table->string('user_avatar');
            $table->string('user_type');
            $table->string('user_about');
            $table->string('user_feedback');
            $table->string('user_verify');
            $table->string('user_verify_level');
            $table->rememberToken();
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('table_users');
	}

}
