<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Diploma extends Model {

    protected $table = 'table_diploma';

    protected $fillable = [
        'user_id',
        'diploma_title',
        'diploma_about',
        'diploma_photo'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

}
