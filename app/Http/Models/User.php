<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model  {


    protected $table = 'table_users';


    protected $fillable = [
        'user_fristname',
        'user_lastname',
        'user_phonenumber',
        'user_email',
        'user_avatar',
        'user_type',
        'user_about',
        'user_feedback',
        'user_verify',
        'user_verify_level',
        'remember_token'
    ];


    protected $hidden = [
        'created_at',
        'updated_at'
    ];

}

