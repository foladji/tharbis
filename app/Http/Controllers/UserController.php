<?php namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Diploma;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {



	public function create(Request $request)
	{

        $validator = Validator::make($request->all(),[
            'user_fristname'       => 'required',
            'user_lastname'        => 'required',
            'user_phonenumber'     => 'required|unique:table_users',
            'user_email'           => 'required|unique:table_users',
            'user_avatar'          => 'string',
            'user_type'            => 'required',
            'user_about'           => 'string',
            'remember_token'       => 'string'
        ]);

        if($validator->fails()) {
            $check = false;
        } else {
            $user = new User;
            $user->user_fristname = $request->input('user_fristname');
            $user->user_lastname = $request->input('user_lastname');
            $user->user_phonenumber = $request->input('user_phonenumber');
            $user->user_email = $request->input('user_email');
            $user->user_avatar = $request->input('user_avatar');
            $user->user_type = $request->input('user_type');
            $user->user_about = $request->input('user_about');
            $user->remember_token = $request->input('user_token');
            $check = $user->save();
        }

		if($check) {
            return response()->json([
                'success' => $check,
                'data' => $user
            ]);
        } else {
            return response()->json([
                'success' => $check
            ]);
        }

	}

    public function createDiploma(Request $request, $user_id)
    {

        $validator = Validator::make($request->all(),[
            'diploma_title' => 'required',
            'diploma_about' => 'required',
            'diploma_photo' => 'required'
        ]);

        if($validator->fails()) {
            $check = false;
        } else {
            $diploma = new Diploma;
            $diploma->user_id = $user_id;
            $diploma->diploma_title = $request->input('diploma_title');
            $diploma->diploma_about = $request->input('diploma_about');
            $diploma->diploma_photo = $request->input('diploma_photo');
            $check = $diploma->save();
        }

        if($check) {
            return response()->json([
                'success' => $check,
                'data' => $diploma
            ]);
        } else {
            return response()->json([
                'success' => $check
            ]);
        }

    }

	public function show($token)
	{
	    $user = new User();
        $result = $user->where('remember_token', '=', $token)->first();

        return response()->json([
            'success' => $result
        ]);
	}

    public function getUser($user_id)
    {
        $user = new User();
        $result = $user->where('user_id', '=', $user_id)->first();

        return response()->json([
            'success' => $result
        ]);
    }

	public function update(Request $request, $user_id)
	{
            User::where('user_id', $user_id)->update( array(
                'user_fristname' 	  =>  $request->input('user_fristname'),
                'user_lastname'       =>  $request->input('user_lastname'),
                'user_email'          =>  $request->input('user_email'),
                'user_avatar'         =>  $request->input('user_avatar'),
                'user_type'           =>  $request->input('user_type'),
                'user_about'          =>  $request->input('user_about')
            ));

        $user = new User();
        $result = $user->where('user_id', '=', $user_id)->first();

            return response()->json([
                'success' => "true",
                'data' => $result
            ]);
	}

    public function updateDiploma(Request $request, $user_id)
    {
        Diploma::where('user_id', $user_id)->update( array(
            'diploma_title' 	  =>  $request->input('diploma_title'),
            'diploma_about'       =>  $request->input('diploma_about'),
            'diploma_photo'       =>  $request->input('diploma_photo')
        ));

        $diploma = new Diploma();
        $result = $diploma->where('user_id', '=', $user_id)->first();

        return response()->json([
            'success' => "true",
            'data' => $result
        ]);
    }

	public function destroy($id)
	{

	}

    public function registeredCheck(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_phonenumber' => 'required|unique:table_users'
        ]);
        if($validator->fails()) {
            return response()->json([
               'check' => 'true',
            ]);
        } else {
            return response()->json([
                'check' => 'false',
            ]);
        }
    }




}
