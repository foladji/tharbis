<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

        Route::get('/', 'WelcomeController@index');

        Route::get('home', 'HomeController@index');

        Route::controllers([
            'auth' => 'Auth\AuthController',
            'password' => 'Auth\PasswordController',
        ]);

        Route::post('/www', 'WelcomeController@www');

        Route::post('/register', 'UserController@create');

        Route::get('/show/{token}', 'UserController@show');

        Route::post('/update/{user_id}', 'UserController@update');

        Route::post('/create_diploma/{user_id}', 'UserController@createDiploma');

        Route::post('/update_diploma/{user_id}', 'UserController@updateDiploma');

        Route::post('/registered_check', 'UserController@registeredCheck');
